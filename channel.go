package main

import (
	"fmt"
	"time"
)

var respch = make(chan string)

func getEmail(fn string , ln string) {
	time.Sleep(time.Millisecond * 100)
	email := fmt.Sprintf("%s.%s@ubs.com", fn, ln)
	respch <- email
	wg.Done()
}

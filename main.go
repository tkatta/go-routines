package main

import (
	"log"
	"math/rand"
	"sync"
	"time"
)

var wg sync.WaitGroup

func doWork(){
	log.Println("started doing work...!")
	time.Sleep(time.Millisecond * 100)
	value := rand.Int()
	log.Printf("work done value:%d", value)
	wg.Done()
}

func notifyUser(){
	log.Println("notifying user....!")
	time.Sleep(time.Millisecond * 150)
	log.Println("sent an email notification to user:", respch)
	wg.Done()
}

func main(){
	start := time.Now()
	wg.Add(3)
	go doWork()
	go getEmail("tinesh", "katta")
	go notifyUser()
	wg.Wait()
	time.Sleep(time.Millisecond * 100)
	log.Println("Program took:", time.Since(start))
}